<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Backend;

class MainController extends Controller
{
    // Create new backend
    public function create(Request $request)
    {
        $props = $request->all();
        $backend = new Backend;
        // foreach($props as $key => $prop){
        //     $backend[$key] = $prop;
        // }
        $backend->name = $props['name'];
        $backend->save();

        foreach($props as $key => $prop){
            if($prop == "laravel"){
                // Create new application based on name
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name']);
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name'].'/app');
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name'].'/app/Http');
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name'].'/app/Http/Controllers');
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name'].'/database');
            File::makeDirectory(base_path().'/public/generated-apps/'.$props['name'].'/database/migrations');

            //Copy the BluePrint generated files
            copy(app_path()."/Http/Controllers/PostController.php",base_path()."/public/generated-apps/".$props['name']."/app/Http/Controllers/PostController.php");
            copy(base_path()."/database/migrations/2020_05_20_085539_create_posts_table.php",base_path()."/public/generated-apps/".$props['name']."/database/migrations/2020_05_20_085539_create_posts_table.php");
            copy(app_path()."/Post.php",base_path()."/public/generated-apps/".$props['name']."/app/Post.php");
            }
            if($prop == "passport"){
                // Create new application based on name
                $process = new Process(['passport.sh']);
                $process->setWorkingDirectory('~/generated-apps/');
                // $process->run();

                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }

                echo $process->getOutput();
            }
        }

        return response()->json($backend);
    }
}
