<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Backend Manager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                /* align-items: center; */
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <h1> List backends to edit</h1>

                <h1> Add existing backend </h1>

                <h1> Create new backend </h1>
                <form method="POST" action="/create">
                    @csrf
                    
                    Name:
                    <input id="name" name="name" type="text" class="@error('name') is-invalid @enderror"> <br/>
                    <br/>
                    Framework: <br/>
                    <input type="checkbox" name="laravel" value="laravel"> Laravel<br/>
                    <input type="checkbox" name="node" value="node"> NodeJS<br/>
                    <br/>
                    Features:<br/>
                    <input type="checkbox" name="api-only" value="api-only"> API Only<br/>
                    <input type="checkbox" name="passport" value="passport"> Passport<br/>
                    <input type="checkbox" name="permissions" value="permissions"> Permissions<br/>
                    <br/>
                    Models:<br/>
                    <input type="checkbox" name="projects" value="projects"> Projects (Add controller customization later for different CRUD)<br/>
                    <input type="checkbox" name="customers" value="customers"> Customers<br/>
                    <input type="checkbox" name="account_managers" value="account_managers"> Account Managers<br/>
                    <input type="checkbox" name="sales_reps" value="sales_reps"> Sales Reps<br/>
                    Add Custom Model<br/><br/>
                    <input type="submit" value="Create">
                </form>
                

                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </body>
</html>
